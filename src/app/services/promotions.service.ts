import { Injectable } from '@angular/core';
import { APIServiceService } from './apiservice.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PromotionsService {

  constructor(private apiService: APIServiceService, private http: HttpClient) { }
  public postCreatePromotion(promotion: any) {
    console.log(promotion);
    const url = "https://lxurioks38.execute-api.ap-south-1.amazonaws.com/dev/createpromo";
//     const headers = new HttpHeaders().set('Content-Type', 'application/json');
//     headers.append('Access-Control-Allow-Origin', '*');
// headers.append('Access-Control-Allow-Credentials', 'true');
// let headers = new HttpHeaders();

//     headers.append('Content-Type', 'application/json');
//     headers.append('Accept', 'application/json');
//     headers.append('Origin','http://localhost:4200');
    const headers = new HttpHeaders({
      'Content-Type' : 'application/json'
    });
    return this.http
      .post(url, JSON.stringify(promotion),{headers: headers});

    // return this.apiService.post<any>(url, JSON.stringify(promotion));
  }
  public getPromotions():Observable<any> {
    const url = "https://lxurioks38.execute-api.ap-south-1.amazonaws.com/dev/createpromo";
    const headers = new HttpHeaders({
      'Content-Type' : 'application/json'
    });
    return this.http.get(url);

    // return this.apiService.post<any>(url, JSON.stringify(promotion));
  }
  public promotionDetails(id: any):Observable<any> {
    const url = "https://lxurioks38.execute-api.ap-south-1.amazonaws.com/dev/createpromo";
    const headers = new HttpHeaders({
      'Content-Type' : 'application/json'
    });
    console.log(url+"/"+id);
    return this.http.get(url+"/"+id);

    // return this.apiService.post<any>(url, JSON.stringify(promotion));
  }

  public getConfiguration():Observable<any> {
    const url = "https://qqj5cghu1e.execute-api.ap-southeast-2.amazonaws.com/dev/vms/config/v1/fetch";
    const headers = new HttpHeaders({
      'Content-Type' : 'application/json',
      'x-api-key': 'N9ZtN8Psyp28a13uFuwsM77xyBLZCDIJEXHyVEk2'     
    });
    return this.http.get(url, {headers});
  }
  public getJSON(): Observable<any> {
    return this.http.get("./assets/promotionconfigresponse4.json");
}
}
