import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdvancedSearchComponent } from './contentComponents/advanced-search/advanced-search.component';
import { ConfirmPromotionComponent } from './contentComponents/confirm-promotion/confirm-promotion.component';
import { CreateVoucherComponent } from './contentComponents/create-voucher/create-voucher.component';
import { EventandpromotionsComponent } from './contentComponents/eventandpromotions/eventandpromotions.component';
import { HomepageComponent } from './contentComponents/homepage/homepage.component';
import { PromotiondetailsComponent } from './contentComponents/promotiondetails/promotiondetails.component';
import { PromotionsearchComponent } from './contentComponents/promotionsearch/promotionsearch.component';
import { ReportsComponent } from './contentComponents/reports/reports.component';
import { SearcheventComponent } from './contentComponents/searchevent/searchevent.component';
import { SearchresultsComponent } from './contentComponents/searchresults/searchresults.component';
import { SrgeventsComponent } from './contentComponents/srgevents/srgevents.component';
import { SrgpromotionComponent } from './contentComponents/srgpromotion/srgpromotion.component';
import { VmsSignInComponent } from './contentComponents/vms-sign-in/vms-sign-in.component';
import { UidesignsComponent } from './layouts/uidesigns/uidesigns.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomepageComponent,
  },
  {
    path: '',
    component: VmsSignInComponent,
  },
  {
    path: 'advancesearch',
    component: AdvancedSearchComponent,
  },
  {
    path: 'createvoucher',
    component: CreateVoucherComponent,
  },
  {
    path: 'searchresults',
    component: SearchresultsComponent
  },
  {
    path: 'uidesigns',
    component: UidesignsComponent
  },
  {
    path: 'reports',
    component: ReportsComponent
  },
  {
    path: 'srgevents',
    component: SrgeventsComponent
  },
  {
    path: 'searchevents',
    component: SearcheventComponent
  },
  {
    path: 'createpromotion',
    component: SrgpromotionComponent
  },
  {
    path: 'eventandpromotions',
    component: EventandpromotionsComponent
  },
  {
    path: 'promotiondetails/:id',
    component: PromotiondetailsComponent
  },
  {
    path: 'promotionsearch',
    component: PromotionsearchComponent
  },
  {
    path: 'vmssignin',
    component: VmsSignInComponent
  },
  {
    path: 'confirmpromotion',
    component: ConfirmPromotionComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
