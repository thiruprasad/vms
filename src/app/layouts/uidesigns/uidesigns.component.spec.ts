import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UidesignsComponent } from './uidesigns.component';

describe('UidesignsComponent', () => {
  let component: UidesignsComponent;
  let fixture: ComponentFixture<UidesignsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UidesignsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UidesignsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
