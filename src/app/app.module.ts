import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './pageComponents/header/header.component';
import { HomepageComponent } from './contentComponents/homepage/homepage.component';
import { LeftNavigationComponent } from './pageComponents/left-navigation/left-navigation.component';
import { FooterComponent } from './pageComponents/footer/footer.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCardModule } from '@angular/material/card';
import { AdvancedSearchComponent } from './contentComponents/advanced-search/advanced-search.component';
import { MatButtonModule } from '@angular/material/button';
import { MatRadioModule } from '@angular/material/radio';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { CreateVoucherComponent } from './contentComponents/create-voucher/create-voucher.component';
import { SearchresultsComponent } from './contentComponents/searchresults/searchresults.component';
import { UidesignsComponent } from './layouts/uidesigns/uidesigns.component';
import { ReportsComponent } from './contentComponents/reports/reports.component';
import { NgChartsModule } from 'ng2-charts';
import { MatDialogModule } from '@angular/material/dialog';
import {MatExpansionModule} from '@angular/material/expansion';
import { ChartType } from 'chart.js';
import { SrgeventsComponent } from './contentComponents/srgevents/srgevents.component';
import { SrgpromotionComponent } from './contentComponents/srgpromotion/srgpromotion.component';
import { SearcheventComponent } from './contentComponents/searchevent/searchevent.component';
import { EventandpromotionsComponent } from './contentComponents/eventandpromotions/eventandpromotions.component';
import { PromotiondetailsComponent } from './contentComponents/promotiondetails/promotiondetails.component';
import { PromotionsearchComponent } from './contentComponents/promotionsearch/promotionsearch.component';
import { AmplifyAuthenticatorModule } from '@aws-amplify/ui-angular';
import { VmsSignInComponent } from './contentComponents/vms-sign-in/vms-sign-in.component';
import { HttpClientModule } from '@angular/common/http';
import { ConfirmPromotionComponent } from './contentComponents/confirm-promotion/confirm-promotion.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomepageComponent,
    LeftNavigationComponent,
    FooterComponent,
    AdvancedSearchComponent,
    CreateVoucherComponent,
    SearchresultsComponent,
    UidesignsComponent,
    ReportsComponent,
    SrgeventsComponent,
    SrgpromotionComponent,
    SearcheventComponent,
    EventandpromotionsComponent,
    PromotiondetailsComponent,
    PromotionsearchComponent,
    VmsSignInComponent,
    ConfirmPromotionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatCardModule,
    MatButtonModule,
    MatRadioModule,
    MatFormFieldModule,
    MatTableModule,
    MatPaginatorModule,
    MatCheckboxModule,
    NgChartsModule,
    MatDialogModule,
    MatExpansionModule,
    AmplifyAuthenticatorModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
