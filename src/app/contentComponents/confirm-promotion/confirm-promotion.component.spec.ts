import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmPromotionComponent } from './confirm-promotion.component';

describe('ConfirmPromotionComponent', () => {
  let component: ConfirmPromotionComponent;
  let fixture: ComponentFixture<ConfirmPromotionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmPromotionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmPromotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
