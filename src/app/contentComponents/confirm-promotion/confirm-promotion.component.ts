import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-confirm-promotion',
  templateUrl: './confirm-promotion.component.html',
  styleUrls: ['./confirm-promotion.component.scss']
})
export class ConfirmPromotionComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  backToPromotion() {
    this.router.navigate(['/createpromotion']);
  }
  searchPromotion(){
    this.router.navigate(['/searchpromotion']);
  }
  home(){
    this.router.navigate(['/home']);
  }
}
