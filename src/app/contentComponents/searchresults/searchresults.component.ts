import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-searchresults',
  templateUrl: './searchresults.component.html',
  styleUrls: ['./searchresults.component.scss']
})
export class SearchresultsComponent implements AfterViewInit {

  constructor() { }

  displayedColumns: string[] = ['voucherid', 'vouchername', 'customerid', 'articleid', 'typeofvouchers', 'status', 'brand', 'startdate', 'enddate'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    
  }

}

export interface PeriodicElement {
  voucherid: string;
  vouchername: string;
  customerid: number;
  articleid: number;
  typeofvouchers: string;
  status : string;
  brand: string;
  startdate: string;
  enddate: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {vouchername: "New year", voucherid: 'Do5432546', customerid: 2546332115, articleid: 3689999122, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
  {vouchername: "New year", voucherid: 'Do5432547', customerid: 8975544444, articleid: 3689999123, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
  {vouchername: "New year", voucherid: 'Do5432548', customerid: 3452654456, articleid: 3689999124, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
  {vouchername: "New year", voucherid: 'Do5432549', customerid: 3456228789, articleid: 3689999125, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
  {vouchername: "New year", voucherid: 'Do5432550', customerid: 3698745661, articleid: 3689999126, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
  {vouchername: "New year", voucherid: 'Do5432551', customerid: 3987456247, articleid: 3689999127, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
  {vouchername: "New year", voucherid: 'Do5432552', customerid: 2345698712, articleid: 3689999128, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
  {vouchername: "New year", voucherid: 'Do5432553', customerid: 3256897455, articleid: 3689999129, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
  {vouchername: "New year", voucherid: 'Do5432554', customerid: 3289745621, articleid: 3689999130, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
  {vouchername: "New year", voucherid: 'Do5432555', customerid: 3698754125, articleid: 3689999131, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
  {vouchername: "New year", voucherid: 'Do5432556', customerid: 3689745851, articleid: 3689999132, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
  {vouchername: "New year", voucherid: 'Do5432557', customerid: 3897455235, articleid: 3689999133, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
  {vouchername: "New year", voucherid: 'Do5432558', customerid: 3256478925, articleid: 3689999134, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
  {vouchername: "New year", voucherid: 'Do5432559', customerid: 2365987451, articleid: 3689999135, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
  {vouchername: "New year", voucherid: 'Do5432560', customerid: 4569823564, articleid: 3689999136, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
  {vouchername: "New year", voucherid: 'Do5432561', customerid: 1365897558, articleid: 3689999137, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
  {vouchername: "New year", voucherid: 'Do5432562', customerid: 2897456356, articleid: 3689999138, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
  {vouchername: "New year", voucherid: 'Do5432563', customerid: 2897456988, articleid: 3689999139, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
  {vouchername: "New year", voucherid: 'Do5432564', customerid: 4568975333, articleid: 3689999140, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
  {vouchername: "New year", voucherid: 'Do5432565', customerid: 8973658996, articleid: 3689999141, typeofvouchers: 'Personalized', status: 'active', brand: 'SRG', startdate: '07-02-2022', enddate: '10-02-2022'},
];