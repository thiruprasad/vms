import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';


@Component({
  selector: 'app-advanced-search',
  templateUrl: './advanced-search.component.html',
  styleUrls: ['./advanced-search.component.scss']
})
export class AdvancedSearchComponent implements OnInit {

  constructor() { }
  selected = 'select';
  startdate = new FormControl(new Date());
  enddate = new FormControl(new Date());
  ngOnInit(): void {
  }

}
