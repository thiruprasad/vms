import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { PromotionsService } from 'src/app/services/promotions.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-promotionsearch',
  templateUrl: './promotionsearch.component.html',
  styleUrls: ['./promotionsearch.component.scss']
})
export class PromotionsearchComponent implements OnInit {
  promotionLists: any;
  

  constructor(private router: Router, private promoDetails:PromotionsService) { }
  selected = 'select';
  startdate = new FormControl(new Date());
  enddate = new FormControl(new Date());
  eventsDrp: string = "";
  eventslist: any;
  filteredPromoTypes: any[] = [];
  listAllEventNames: any[] = [];
  filterdEvnts: any[] = [];
  eventss: any[] = [];
  filteredPromotions: any[] = [];
  tableShow: boolean = false;
  disablePromo: boolean = true;
  displayedColumns: string[] = [ 
    'type', 
    'id', 
    'status', 
    'brand',
    'overview', 
    'channel',
    'redeemed',
    'startdate',
    'enddate',
    'promochannel',
    'articleid',
    'discounttype',
    'discountamount',
    'minimumspend',
    'coupondescription',
    'receiptdescription',
    'termsandconditions',
    'promotionregionlevel',
    'promotionregioncountry',
    'promotionregionorstate',
    'promotionregionstoreid',
    'pospromotionid',
    'advertisingregionlevel',
    'advertisingregioncountry',
    'advertisingregionorstate',
    'advertisingregionstoreid',
    'advertisingpromotionid'
  ];
  promotions: any[] = [
    {
      "promotionType": "Personalized",
      "promotionID": "Promo001",
      "promotionStatus": "Active",
      "promotionOverview": "Overview Text",
      "advertisingChannel": "SMS",
      "redeemedby": "Any Member",
      "startDate": "01-01-2021",
      "endDate": "15-01-2021",
      "promotionalChannel": "Instore",
      "articleID": "ART001",
      "discountType": "$10 Birthday Coupon",
      "discountAmount":"$10",
      "minimumSpend": "100",
      "couponDescription": "Coupon description",
      "receiptDescription": "Receipt Description",
      "termsandconditions": "Terms and Conditions",
      "promoregionLevel": "Store",
      "promoregionorstate": "NorthWales",
      "promoregionstoreid": "NWL001",
      "pospromotionId": "",
      "advertisingregionLevel": "",
      "advertisingregionorstate": "",
      "advertisingregionstoreid": "",
      "advertisingpromotionId": ""
    },
    {
      "promotionType": "Personalized",
      "promotionID": "Promo002",
      "promotionStatus": "Inactive",
      "promotionOverview": "Overview Text",
      "advertisingChannel": "EMAIL",
      "redeemedby": "Anyone",
      "startDate": "16-01-2021",
      "endDate": "30-01-2021",
      "promotionalChannel": "Instore and Online",
      "articleID": "ART002",
      "discountType": "$20 Birthday Coupon",
      "discountAmount":"$20",
      "minimumSpend": "200",
      "couponDescription": "Coupon description",
      "receiptDescription": "Receipt Description",
      "termsandconditions": "Terms and Conditions",
      "promoregionLevel": "Store",
      "promoregionorstate": "North Wales",
      "promoregionstoreid": "NWL002",
      "pospromotionId": "",
      "advertisingregionLevel": "",
      "advertisingregionorstate": "",
      "advertisingregionstoreid": "",
      "advertisingpromotionId": ""
    },
    {
      "promotionType": "Personalized",
      "promotionID": "Promo003",
      "promotionStatus": "Inactive",
      "promotionOverview": "Overview Text",
      "advertisingChannel": "EMAIL",
      "redeemedby": "Anyone",
      "startDate": "01-02-2021",
      "endDate": "15-02-2021",
      "promotionalChannel": "Instore",
      "articleID": "ART003",
      "discountType": "$30 Birthday Coupon",
      "discountAmount":"$30",
      "minimumSpend": "300",
      "couponDescription": "Coupon description",
      "receiptDescription": "Receipt Description",
      "termsandconditions": "Terms and Conditions",
      "promoregionLevel": "Store",
      "promoregionorstate": "NorthWales",
      "promoregionstoreid": "NWL003",
      "pospromotionId": "",
      "advertisingregionLevel": "",
      "advertisingregionorstate": "",
      "advertisingregionstoreid": "",
      "advertisingpromotionId": ""
    },
    {
      "promotionType": "Personalized",
      "promotionID": "Promo004",
      "promotionStatus": "Active",
      "promotionOverview": "Overview Text",
      "advertisingChannel": "EMAIL",
      "redeemedby": "Any Member",
      "startDate": "16-02-2021",
      "endDate": "28-02-2021",
      "promotionalChannel": "Online",
      "articleID": "ART004",
      "discountType": "$40 Birthday Coupon",
      "discountAmount":"$40",
      "minimumSpend": "400",
      "couponDescription": "Coupon description",
      "receiptDescription": "Receipt Description",
      "termsandconditions": "Terms and Conditions",
      "promoregionLevel": "Store",
      "promoregionorstate": "NorthWales",
      "promoregionstoreid": "NWL004",
      "pospromotionId": "",
      "advertisingregionLevel": "",
      "advertisingregionorstate": "",
      "advertisingregionstoreid": "",
      "advertisingpromotionId": ""
    },
    {
      "promotionType": "Personalized",
      "promotionID": "Promo005",
      "promotionStatus": "Active",
      "promotionOverview": "Overview Text",
      "advertisingChannel": "SMS",
      "redeemedby": "Anyone",
      "startDate": "01-03-2021",
      "endDate": "15-03-2021",
      "promotionalChannel": "Instore and Online",
      "articleID": "ART005",
      "discountType": "$50 Birthday Coupon",
      "discountAmount":"$50",
      "minimumSpend": "500",
      "couponDescription": "Coupon description",
      "receiptDescription": "Receipt Description",
      "termsandconditions": "Terms and Conditions",
      "promoregionLevel": "Store",
      "promoregionorstate": "NorthWales",
      "promoregionstoreid": "NWL005",
      "pospromotionId": "",
      "advertisingregionLevel": "",
      "advertisingregionorstate": "",
      "advertisingregionstoreid": "",
      "advertisingpromotionId": ""
    },
    {
      "promotionType": "Personalized",
      "promotionID": "Promo006",
      "promotionStatus": "Inactive",
      "promotionOverview": "Overview Text",
      "advertisingChannel": "SMS",
      "redeemedby": "Any Member",
      "startDate": "16-03-2021",
      "endDate": "30-03-2021",
      "promotionalChannel": "Instore",
      "articleID": "ART006",
      "discountType": "$60 Birthday Coupon",
      "discountAmount":"$60",
      "minimumSpend": "600",
      "couponDescription": "Coupon description",
      "receiptDescription": "Receipt Description",
      "termsandconditions": "Terms and Conditions",
      "promoregionLevel": "Store",
      "promoregionorstate": "NorthWales",
      "promoregionstoreid": "NWL006",
      "pospromotionId": "",
      "advertisingregionLevel": "",
      "advertisingregionorstate": "",
      "advertisingregionstoreid": "",
      "advertisingpromotionId": ""
    },
    {
      "promotionType": "Personalized",
      "promotionID": "Promo007",
      "promotionStatus": "Active",
      "promotionOverview": "Overview Text",
      "advertisingChannel": "EMAIL",
      "redeemedby": "Any Member",
      "startDate": "01-04-2021",
      "endDate": "15-04-2021",
      "promotionalChannel": "Online",
      "articleID": "ART007",
      "discountType": "$70 Birthday Coupon",
      "discountAmount":"$70",
      "minimumSpend": "700",
      "couponDescription": "Coupon description",
      "receiptDescription": "Receipt Description",
      "termsandconditions": "Terms and Conditions",
      "promoregionLevel": "Store",
      "promoregionorstate": "NorthWales",
      "promoregionstoreid": "NWL007",
      "pospromotionId": "",
      "advertisingregionLevel": "",
      "advertisingregionorstate": "",
      "advertisingregionstoreid": "",
      "advertisingpromotionId": ""
    },
    {
      "promotionType": "Personalized",
      "promotionID": "Promo008",
      "promotionStatus": "Inactive",
      "promotionOverview": "Overview Text",
      "advertisingChannel": "SMS",
      "redeemedby": "Anyone",
      "startDate": "16-04-2021",
      "endDate": "30-04-2021",
      "promotionalChannel": "Instore and Online",
      "articleID": "ART008",
      "discountType": "$80 Birthday Coupon",
      "discountAmount":"$80",
      "minimumSpend": "800",
      "couponDescription": "Coupon description",
      "receiptDescription": "Receipt Description",
      "termsandconditions": "Terms and Conditions",
      "promoregionLevel": "Store",
      "promoregionorstate": "NorthWales",
      "promoregionstoreid": "NWL008",
      "pospromotionId": "",
      "advertisingregionLevel": "",
      "advertisingregionorstate": "",
      "advertisingregionstoreid": "",
      "advertisingpromotionId": ""
    },
    {
      "promotionType": "Personalized",
      "promotionID": "Promo009",
      "promotionStatus": "Inactive",
      "promotionOverview": "Overview Text",
      "advertisingChannel": "SMS",
      "redeemedby": "Anyone",
      "startDate": "01-05-2021",
      "endDate": "15-05-2021",
      "promotionalChannel": "Instore",
      "articleID": "ART009",
      "discountType": "$90 Birthday Coupon",
      "discountAmount":"$90",
      "minimumSpend": "900",
      "couponDescription": "Coupon description",
      "receiptDescription": "Receipt Description",
      "termsandconditions": "Terms and Conditions",
      "promoregionLevel": "Store",
      "promoregionorstate": "NorthWales",
      "promoregionstoreid": "NWL009",
      "pospromotionId": "",
      "advertisingregionLevel": "",
      "advertisingregionorstate": "",
      "advertisingregionstoreid": "",
      "advertisingpromotionId": ""
    },
    {
      "promotionType": "Personalized",
      "promotionID": "Promo010",
      "promotionStatus": "Active",
      "promotionOverview": "Overview Text",
      "advertisingChannel": "EMAIL",
      "redeemedby": "Any Member",
      "startDate": "16-05-2021",
      "endDate": "30-05-2021",
      "promotionalChannel": "Online",
      "articleID": "ART010",
      "discountType": "$100 Birthday Coupon",
      "discountAmount":"$100",
      "minimumSpend": "1000",
      "couponDescription": "Coupon description",
      "receiptDescription": "Receipt Description",
      "termsandconditions": "Terms and Conditions",
      "promoregionLevel": "Store",
      "promoregionorstate": "NorthWales",
      "promoregionstoreid": "NWL010",
      "pospromotionId": "",
      "advertisingregionLevel": "",
      "advertisingregionorstate": "",
      "advertisingregionstoreid": "",
      "advertisingpromotionId": ""
    }
  ]
  brandDrpValues: any[] = ["Spotlight-AU", "Spotlight-NZ", "Spotlight-MY", "Spotlight-SG", "Anaconda-AU", "Mountain Design-AU", "Harris Scarfe-AU"]
  eventNameDrpValues: any[] = ["Christmas", "New Year"];
  promotionDetails: any[] = [];
  ngOnInit(): void {
  
   this.promoDetails.getPromotions().subscribe(
    data => {
      if(data) {
       // JSON.parse(data);
      this.promotionDetails = data.promotions;
      this.filterEvents();
     // console.log(data.promotions);
      this.promotionDetails.forEach((promo) => {
      //  console.log(promo);
     });
    }
    },
    error => {
      console.log(error);
    });

    

 
  console.log(this.eventsDrp);
  if(this.eventsDrp == "") {
    this.tableShow = false;
  } else {
    this.tableShow = true;
  }

  }

  promodetails(id: any){
    console.log(id.$oid);
    this.router.navigate(['/promotiondetails/'+ id.$oid]);
  }

  onlyUnique(value: any, index: any, self: string | any[]) {
    return self.indexOf(value) === index;
  }
  
  filterEvents() {
     // console.log(this.promotionDetails);
      this.promotionDetails.forEach((promotionsList) => {
        const promoD = JSON.parse(promotionsList);
        console.log(promoD.promotionType);
        this.promotionLists = JSON.parse(promotionsList);
        this.listAllEventNames.push(promoD.promotionType);
      })
      console.log(this.listAllEventNames);
      var unique = this.listAllEventNames.filter(this.onlyUnique);
      console.log(unique);    
      this.filteredPromoTypes = unique;
      console.log(this.filteredPromoTypes);
  }

  onChangeEventID(event: any) {
    this.tableShow = true;
    this.disablePromo = false;
    console.log(this.eventsDrp);
    this.eventss = [];
    console.log(this.promotionDetails);
    this.promotionDetails.forEach((promotionsList) => {
      console.log(promotionsList);
      const promoD = JSON.parse(promotionsList);
      console.log(promoD.promotionType);
      this.promotionLists = JSON.parse(promotionsList);
      this.eventss.push(promoD);
    })
   // this.eventss = this.promotions;
    this.filteredPromotions = this.eventss.filter(
      item => item.promotionType.toLowerCase().includes(this.eventsDrp.toLowerCase())
    );
    console.log(this.filteredPromotions);
   // this.eventss = this.filterdEvnts;
  }

  navigateTo() {
    this.router.navigate(['/eventandpromotions']);
  } 

  
  // filterEvnts() {
  //   this.filterdEvnts = this.events.filter(
  //     item => item.eventName.toLowerCase().includes(this.selectedUser.toLowerCase())
  //   );
  //   console.log(this.filterdOptions);
  // }

}
