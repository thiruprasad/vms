import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TemplateRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-srgevents',
  templateUrl: './srgevents.component.html',
  styleUrls: ['./srgevents.component.scss']
})
export class SrgeventsComponent implements OnInit {
  
  constructor(private dialog: MatDialog) { }
  @ViewChild('callAPIDialog')
  callAPIDialog!: TemplateRef<any>;
  selectedDrp = 'srg';
  countryDrp = "australia";
  newPromo: boolean = false;
  brandDrp: string = "";
  extentDaysValue: any[] = ["1 to 7 Days"];
  tolerantDaysValue: any[] = ["14 to 30 Days"];
  brandDrpValues: any[] = ["Spotlight-AU", "Spotlight-NZ", "Spotlight-MY", "Spotlight-SG", "Anaconda-AU", "Mountain Design-AU", "Harris Scarfe-AU"]
  startdate = new FormControl(new Date());
  enddate = new FormControl(new Date());
  saveEvent: boolean = true;
  promoFromEvent: boolean = false;
  brandFromEvent: string = "";
  eventCodeFromEvent: string = "";
  eventCodeValue: string = "";
  eventNameFromEvent: string = "";
  eventNameValue: string = "";
  eventExtendableValue: string = "";
  eventToleranceValue: string = "";
  eventExtendableFromEvent: string = "";
  eventToleranceFromEvent: string = "";
  eventStartDateFromEvent = new FormControl(new Date());
  eventEndDateFromEvent= new FormControl(new Date());

  ngOnInit(): void {
    this.brandDrp = this.brandDrpValues[0];
  }
  
  addNewPromo() {
    this.newPromo = true;
    this.promoFromEvent = true;
    this.brandFromEvent = this.brandDrp;
    this.eventCodeFromEvent = this.eventCodeValue;
    this.eventNameFromEvent = this.eventNameValue;
    this.eventExtendableFromEvent = this.eventExtendableValue;
    this.eventToleranceFromEvent = this.eventToleranceValue;
    this.startdate = this.eventStartDateFromEvent;
    this.enddate = this.eventEndDateFromEvent;
  }
  onChangeBrand(event: any) {
    console.log(this.brandDrp);
  }
  eventNo() {
    this.saveEvent = true;
  }
  eventYes() {
    this.saveEvent = false;
  }
  onSaveEvent() {
    
    let dialogRef = this.dialog.open(this.callAPIDialog);
        dialogRef.afterClosed().subscribe(result => {
            // Note: If the user clicks outside the dialog or presses the escape key, there'll be no result
            if (result !== undefined) {
                if (result === 'yes') {
                    // TODO: Replace the following line with your code.
                    console.log('User clicked yes.');
                } else if (result === 'no') {
                    // TODO: Replace the following line with your code.
                    console.log('User clicked no.');
                }
            }
        })
  }
}
