import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CognitoService } from 'src/app/services/cognito.service';

@Component({
  selector: 'app-vms-sign-in',
  templateUrl: './vms-sign-in.component.html',
  styleUrls: ['./vms-sign-in.component.scss']
})
export class VmsSignInComponent implements OnInit {
  loading: boolean;
  user: any;
  userdetails:any;
  constructor(private router: Router, private cognito:CognitoService) { 
    this.loading = false;
    this.user = {};
  }

  ngOnInit(): void {
    
  }

  public signIn(): void {
    this.loading = true;
    this.cognito.signIn(this.user)
    .then(() => {
      this.router.navigate(['/home']);
    }).catch(() => {
      this.loading = false;
    });
  }

}
