import { Component, OnInit } from '@angular/core';
import { ChartType } from 'chart.js';
@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  }

  public barChartLabels = ["2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022"];
  public barChartType:ChartType = 'bar';
  public barChartLegend = true;

  public barChartData = [
    { 
      data: [10, 20, 30, 40, 50, 60, 70, 80, 90],
      label: 'Online Redeemed'
    },
    { 
      data: [15, 25, 35, 45, 55, 65, 75, 85, 95],
      label: 'In-Store Redeemed'
    }

  ]


  public doughChartType:ChartType = 'doughnut';
  public doughChart = {
    datasets: [{
        data: [10000, 20000, 30000]
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: [
        'Total Vouchers',
        'Redeemed Vouchers',
        'Active Vouchers'
    ]
};

  constructor() { }

  ngOnInit(): void {
  }

}
