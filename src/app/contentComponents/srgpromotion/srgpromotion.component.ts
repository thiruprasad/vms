import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import {ThemePalette} from '@angular/material/core';
import { RouterModule, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { PromotionsService } from 'src/app/services/promotions.service';

@Component({
  selector: 'app-srgpromotion',
  templateUrl: './srgpromotion.component.html',
  styleUrls: ['./srgpromotion.component.scss']
})
export class SrgpromotionComponent implements OnInit {
  adlevelcountry: any;
  adlevelstate: boolean = false;
  adlevelstore: boolean = false;
  
  constructor(private promotionService: PromotionsService, private router: Router) { }

  selectedDrp: string = 'srg';
  countryDrp: string = "";
  articleDrp: string = 'article001';
  statusDrp: string = "active";
  channelDrp: string = "Instore";
  brandDrp: string = "Select brand";
  channelDrpValues: any[] = ["Instore", "Online", "Instore and Online"];
  adChannelDrp: string = "email";
  promoTypeDrp: string = "";
  enforceDrp: string = "anyone";
  redeemedBy: string = "";
  brandDrpValues: any[] = ["Select Brand/Country", "Spotlight-AU", "Spotlight-NZ", "Spotlight-MY", "Spotlight-SG", "Anaconda-AU", "Mountain Design-AU", "Harris Scarfe-AU"]
  adChannelDrpValues: any[] = ["SMS", "Email", "Direct Mail", "Instore", "Letter Box", "Instore Flyer", "Press Ad", "Magazine", "Social", "Website", "Affiliates", "3rd Party" ]
  promotionLevel: any[] = ["Store", "State", "Country"];
  adLevel: any[] = ["Store", "State", "Country"];
  promoLevelDrp: any;
  adLevelDrp: any;
  newPromo: boolean = false;
  startdate = new FormControl(new Date());
  enddate = new FormControl(new Date());
  statesDrp: string = "";
  adLocationState: string = "";
  storesDrp: string = "";
  changedStates: any[] = [];
  changedStores: any[] = [];
  ecompromoid: any;
  selectOnline: boolean = true;
  selectRegionEcom: boolean = true;
  selectRegionPOS: boolean = true;
  redeem: boolean = true;
  abc: any[] = [];
  storeShow: boolean = false;
  stateChecked: any;
  stateunChecked: any;
  promolevelstate: boolean = false;
  promolevelstore: boolean = false
  count: string = "Australia";
  auStates: any[] = ["New South Wales",
  "Queensland",
  "South Australia",
  "Tasmania",
  "Victoria",
  "Western Australia",
  "Australian Capital Territory",
  "Northern Territory",
  "Jervis Bay",
  "Ashmore and Cartier Islands",
  "Christmas Island",
  "Cocos Islands",
  "Coral Sea Islands",
  "Heard Island",
  "McDonald Islands",
  "Norfolk Island",
  "Australian Antarctic Territory" ];

  nzStates: any[] = [
    "Auckland",
    "Canterbury",
    "Wellington",
    "Waikato",
    "Bay of Plenty",
    "Manawatu-Wanganui",
    "Otago",
    "Hawke's Bay",
    "Northland",
    "Taranaki",
    "Southland",
    "Nelson",
    "Gisborne",
    "Marlborough",
    "Tasman",
    "West Coast"
  ];

  storeIds: any[] = [
    {
      "New South Wales": ["NSWOO1", "NSWOO2", "NSWOO3", "NSWOO4", "NSWOO5"]
    },
    {
      "Queensland": ["QLNDOO1", "QLNDOO2", "QLNDOO3", "QLNDOO4", "QLNDOO5"]
    },
    {
      "South Australia": ["SAOO1", "SAOO2", "SAOO3", "SAOO4", "SAOO5"]
    },
    {
      "Auckland": ["AUCOO1", "AUCOO2", "AUCOO3", "AUCOO4", "AUCOO5"]
    },
    {
      "Canterbury": ["CANTOO1", "CANTOO2", "CANTOO3", "CANTOO4", "CANTOO5"]
    }
  ];
  panelOpenState = false;
  storeIdss: string[] = ["NSWOO1", "NSWOO2", "NSWOO3", "NSWOO4", "NSWOO5" ];
  promoLevelStore: any = "";
  adLocationStore: any = "";
  countryAndStores: any[] = [
    {
      "country": "Australia",
      "state":  "New South Wales",
       "stores" : ["NSWOO1", "NSWOO2", "NSWOO3", "NSWOO4", "NSWOO5"]
    },
    {
      "country": "Australia",
      "state": "Queensland",
      "stores": ["QLNDOO1", "QLNDOO2", "QLNDOO3", "QLNDOO4", "QLNDOO5"]
    },
    {
      "country": "Australia",
      "state": "South Australia",
      "stores": ["SAOO1", "SAOO2", "SAOO3", "SAOO4", "SAOO5"]
    },
    {
      "country": "New Zealand",
      "state": "Auckland",
      "stores": ["AUCOO1", "AUCOO2", "AUCOO3", "AUCOO4", "AUCOO5"]
    },
    {
      "country": "New Zealand",
      "state": "Canterbury",
      "stores": ["CANTOO1", "CANTOO2", "CANTOO3", "CANTOO4", "CANTOO5"]
    }
  ];
  createPromotionForm: FormGroup = new FormGroup({});
  promolevelcountry: string = "";
  promolevelcountries: boolean = false;
  adlevelcountries: boolean = false;
  promoOverview: string = "Promotion Overview";
  onlinePromoCode: string = "235689";
  articleId: string = "235689";
  discountType: string = "$10 off";
  discountTypeValue: any[] = ['Dollar Off', 'Percentage Off'];
  discountAmount: string = "10";
  discountPercent: number = 20;
  minimumSpend: string = "100";
  couponDescription: string = "Coupon Description";
  receiptDescription: string = "Receipt Description";
  termsAndConditions: string = "Terms and Conditions";
  country: string = "";
  adLocationCountry: string = "";
  posPromoCode: string = "";
  ecomPromoCode: string = "";
  eventName: string = "";
  eventCode: string = "";
  extendableDays: string = "";
  toleranceDays: string = "";
  ifFromEvent: boolean = false;
  @Input() fromEvent: boolean = false;
  @Input() brand: string = "";
  @Input() eventCodeFromEvent: string = "";
  @Input() eventNameFromEvent: string = "";
  @Input() eventExtendableFromEvent: string = "";
  @Input() eventToleranceFromEvent: string = "";
  @Input() eventStartDateFromEvent = new FormControl(new Date());
  @Input() eventEndDateFromEvent = new FormControl(new Date());
  extentDaysValue: any[] = ["1 to 7 Days"];
  tolerantDaysValue: any[] = ["14 to 30 Days"];

  getPromoType: any[] = [];
  getPromType: any[] = [];
  getPromoTypeValue: any[] = [];

  getPromoStatus: any[] = [];
  getPromoStatusValues: any[] = [];

  getRedeemedBy: any[] = [];
  getRedeemedByValues: any[] = [];

  getSalesChannel: any[] = [];
  getSalesChannelValues: any[] = [];

  getDiscountType: any[] = [];
  getDiscountTypeValues: any[] = [];

  getAdvertisingChannel: any[] = [];
  getAdvertisingChannelValues: any[] = [];

  getExtendible: any[] = [];
  getExtendibleValues: any[] = [];
  getToleranceValues: any[] = [];

  getCountry:any[] = [];
  getCountryValues: any[] = [];

  getBrand: any[] = [];
  getBrandValues: any[] = [];

  getStates: any[] = [];
  getStatesValues: any[] = [];

  getStores: any[] = [];
  getStoresValues: any[] = [];

  percentOff: boolean = false;
  allStates: boolean = true;

  //validation patterns

  alphaneumaric = "^[a-zA-Z0-9]{10}$";
  onlychars = "^[a-zA-Z]+$";
  onlynumbers = "^[0-9]*$";

  ngOnInit(): void {
  
    this.getDefaultValues();
  
    this.createPromotionForm = new FormGroup({
      promotionType: new FormControl(this.promoTypeDrp, [Validators.required]),
      promotionStatus: new FormControl(this.statusDrp, [Validators.required]),
      brand: new FormControl(this.brandDrp, [Validators.required]),
      promotionOverview: new FormControl(this.promoOverview, [Validators.required, Validators.pattern(this.onlychars)]),
      advertisingChannel: new FormControl(this.adChannelDrp, [Validators.required]),
      redeemedBy: new FormControl(this.redeemedBy, [Validators.required]),
      startDate: new FormControl(this.startdate.value.toString(), [Validators.required]),
      endDate: new FormControl(this.enddate.value.toString(), [Validators.required]),
      promotionChannel: new FormControl(this.channelDrp, [Validators.required, Validators.pattern(this.alphaneumaric)]),
      onlinePromoCode: new FormControl(this.onlinePromoCode, [Validators.required]),
      articleId: new FormControl(this.articleId, [Validators.required, Validators.pattern(this.onlynumbers)]),
      discountType: new FormControl(this.discountType, [Validators.required]),
      discountAmount: new FormControl(this.discountAmount, [Validators.required]),
      minimumSpend: new FormControl(this.minimumSpend, [Validators.required]),
      couponDescription: new FormControl(this.couponDescription, [Validators.required, Validators.minLength(15), Validators.maxLength(20)]),
      receiptDescription: new FormControl(this.receiptDescription, [Validators.required, Validators.minLength(15), Validators.maxLength(20)]),
      termsAndConditions: new FormControl(this.termsAndConditions, [Validators.required, Validators.minLength(15), Validators.maxLength(20)]),
      promotionLocationLevel: new FormControl(this.promoLevelDrp, [Validators.required]),
      promotionLocationCountry: new FormControl(this.country, [Validators.required]),
      promotionLocationState: new FormControl(this.statesDrp, [Validators.required]),
      promotionLocationStore: new FormControl(this.promoLevelStore, [Validators.required]),
      posPromoCode: new FormControl(this.posPromoCode, [Validators.required, Validators.pattern(this.alphaneumaric)]),
      advertisingLocationLevel: new FormControl(this.adLevelDrp, [Validators.required]),
      advertisingLocationCountry: new FormControl(this.adLocationCountry, [Validators.required]),
      advertisingLocationState: new FormControl(this.adLocationState, [Validators.required]),
      advertisingLocationStore: new FormControl(this.adLocationStore, [Validators.required]),
      ecomPromoCode: new FormControl(this.ecomPromoCode, [Validators.required, Validators.pattern(this.alphaneumaric)])
    });


    console.log(this.discountType);






  
    if(this.fromEvent == true) {
      console.log(this.brand);
      console.log(this.brandDrp);
      this.brandDrp = this.brand;
      this.eventCode = this.eventCodeFromEvent;
      this.eventName = this.eventNameFromEvent;
      this.extendableDays = this.eventExtendableFromEvent;
      this.toleranceDays = this.eventToleranceFromEvent;
      this.startdate = this.eventStartDateFromEvent;
      this.enddate = this.eventEndDateFromEvent;
      this.ifFromEvent = true;
      console.log(this.brandDrp);
    } else {
      this.brandDrp = this.brandDrpValues[0];
    }
   this.changedStates = this.auStates;
   this.adChannelDrp = this.adChannelDrpValues[0];
   this.statesDrp = this.changedStates[0];
   this.changedStores = this.storeIds;
   this.storesDrp = this.changedStores[0][this.statesDrp][0];
   this.ecompromoid = "";
   this.promoLevelDrp = this.promotionLevel[2];
   this.adLevelDrp = this.adLevel[2];
     
   if(this.ecompromoid == "") {
    this.statusDrp = this.getPromoStatusValues[0];
   } else {
    this.statusDrp = this.getPromoStatusValues[1];
   }

   if(this.promoTypeDrp == "Personalized") {
     this.redeem = false;
   }

   if(this.stateChecked == true) {
     this.storeShow = true;
   }

   this.stateChecked = -1;
   this.stateunChecked = -1;
   if(this.brandDrp == "Spotlight-AU") {
     this.promolevelcountry = "Australia";
     this.adlevelcountry = "Australia";
     this.promolevelcountries = true;
     this.adlevelcountries = true;
   }
  }

  getDefaultValues() {
    this.promotionService.getConfiguration().subscribe(data => {
      console.log(data); 
    });
  this.promotionService.getJSON().subscribe(data => {
    console.log(data.systemConfig);

    this.getPromoType = data.systemConfig.configMap.PROMOTION_TYPE;
    if(this.getPromoType) {
      this.getPromoType.forEach((promoT) => {
         this.getPromoTypeValue.push(promoT.value);
         if(promoT.isDefault == true) {
          let pt = promoT.value;
          this.promoTypeDrp = promoT.value;
         }
      });
     }

     this.getPromoStatus = data.systemConfig.configMap.PROMOTION_STATUS;
    // console.log(this.getPromoStatus);
     if(this.getPromoStatus) {
        this.getPromoStatus.forEach((promoS) => {
          this.getPromoStatusValues.push(promoS.value);
          if(promoS.isDefault == true) {
            let ps = promoS.value;
            this.statusDrp = ps;
          }
        });
     }

     this.getRedeemedBy = data.systemConfig.configMap.REDEEMABLE_BY;
     console.log(this.getRedeemedBy);
     if(this.getRedeemedBy) {
        this.getRedeemedBy.forEach((redemBy) => {
          this.getRedeemedByValues.push(redemBy.value);
          if(redemBy.isDefault == true) {
            let ps = redemBy.value;
            this.redeemedBy = ps;
          }
        });
     }

     this.getSalesChannel = data.systemConfig.configMap.SALES_CHANNEL;
     console.log(this.getSalesChannel);
     if(this.getSalesChannel) {
        this.getSalesChannel.forEach((salesC) => {
          this.getSalesChannelValues.push(salesC.value);
          if(salesC.isDefault == true) {
            let ps = salesC.value;
            this.channelDrp = ps;
          }
        });
     }

     this.getDiscountType = data.systemConfig.configMap.DISCOUNT_TYPE;
     console.log(this.getDiscountType);
     if(this.getDiscountType) {
        this.getDiscountType.forEach((discountT) => {
          this.getDiscountTypeValues.push(discountT.value);
          if(discountT.isDefault == true) {
            let ps = discountT.value;
            this.discountType = ps;
          }
          if(discountT.value == "% Off") {
            this.percentOff = true;
          } else {
            this.percentOff = false;
          }
        });
     }

     this.getAdvertisingChannel = data.userConfig.configMap.ADVERTISING_CHANNEL;
     console.log(this.getAdvertisingChannel);
     if(this.getAdvertisingChannel) {
        this.getAdvertisingChannel.forEach((advertisingC) => {
          this.getAdvertisingChannelValues.push(advertisingC.value);
          if(advertisingC.isDefault == true) {
            let ps = advertisingC.value;
            this.adChannelDrp = ps;
          }
        });
     }

     this.getExtendible = data.userConfig.configMap.CONFIG;
     console.log(this.getExtendible);
     if(this.getExtendible) {
        this.getExtendible.forEach((extendibleD) => {
          if(extendibleD.code == "EXTENDABLEDAYS") {
            var totaldays = extendibleD.value;
            for(var i=1; i <= totaldays; i++ ){
            this.getExtendibleValues.push(i);
            }
            this.extendableDays = this.getExtendibleValues[(totaldays)-1];
          }
          if(extendibleD.code == "TOLERANCEDAYS") {
            var totaldays = extendibleD.value;
            for(var i=1; i <= totaldays; i++ ){
            this.getToleranceValues.push(i);
            }
            this.toleranceDays = this.getToleranceValues[(totaldays)-1];
          }
          
        });
     }

     this.getCountry = data.userConfig.sourceMarket;
     console.log(this.getCountry);
     if(this.getCountry) {
        this.getCountry.forEach((country) => {
        this.getCountryValues.push(country.country);
          if(country.isDefault == true) {
            let ps = country;
            this.countryDrp = ps;
          }
        });
     }

   });  
}
 
  onChange(event: any) {
    console.log(event)
    console.log(this.stateChecked);
    console.log(this.promoLevelStore);
   // this.stateChecked = -1;
  }

  onChangeChannel(event: any) {
    console.log(this.channelDrp);
    if(this.channelDrp == "Instore") {
      this.selectOnline = false;
      this.selectRegionEcom = false;
      this.selectRegionPOS = true;
    } else if(this.channelDrp == "Online"){
      this.selectOnline = true;
      this.selectRegionEcom = true;
      this.selectRegionPOS = false;
    } else if(this.channelDrp == "Instore and Online"){
      this.selectOnline = true;
      this.selectRegionEcom = true;
      this.selectRegionPOS = true;
    }
  }
  onChangeCountry(event: any) {
    console.log(event.value);
    this.getBrandValues = event.value.brand;
    console.log(this.getBrandValues);
  }

  onDiscountType(event: any) {
    console.log(event.value);
    if(event.value == "% Off") {
      this.percentOff = false;
    } else {
      this.percentOff = true;
    }
  }

  onChangeBrand(event: any) {
    this.getStatesValues = event.value.states;
    this.allStates = false;
  }
  onChangeBrands(event: any) {
   console.log(event.value);
    this.statesDrp = "";
    this.changedStores = [];
    this.changedStores = this.storeIds;
   console.log(this.getBrandValues);
    if(this.brandDrp == 'Spotlight-NZ') {
      this.changedStates = this.nzStates;
      this.promolevelcountry = "New Zealand";
      this.adlevelcountry = "New Zealand";
      this.changedStores.forEach((sid) => {
        this.statesDrp = this.changedStates[0];
        var state = Object.keys(sid).toString();
        if(state == this.statesDrp) {
          this.changedStores = [];
          this.changedStores.push(sid);
        }
      });
    }
    if(this.brandDrp == 'Spotlight-AU') {
      this.changedStates = this.auStates;
      this.promolevelcountry = "Australia";
      this.adlevelcountry = "Australia";
      this.changedStores.forEach((sid) => {
        console.log(sid);
        this.statesDrp = this.changedStates[0];
        var state = Object.keys(sid).toString();
        if(state == this.statesDrp) {
          this.changedStores = [];
          this.changedStores.push(sid);
        }
      });
    }
    this.statesDrp = this.changedStates[0];
    this.storesDrp = this.changedStores[0][this.statesDrp][0];
  }
  onChangeAdChannel(event: any) {

  }
  onChangeRedeemedBy(event: any) {

  }
  onChangeStates(event: any) {
  //  var states = this.statesDrp;
  
    this.storeIds.forEach((sid) => {
      this.storesDrp = "";
     // this.statesDrp = "";
   // console.log(sid);
   // console.log(this.statesDrp);
    
      var state = Object.keys(sid).toString();
     // console.log(state);
     // console.log(states);
      if(state == this.statesDrp) {
        this.changedStores = [];
     // console.log(sid);
     // console.log(this.statesDrp);
       this.changedStores.push(sid);
     //  console.log(this.changedStores[0][this.statesDrp][0]);
     // this.statesDrp = this.changedStores[0];
     // this.abc = this.changedStores[0][this.statesDrp];
    
    this.statesDrp = this.changedStates[0];
    
    console.log(this.storesDrp);
    console.log(this.statesDrp);
      }
     // this.storesDrp = this.changedStores[0][this.statesDrp][0];
    });
    
  }
  // onChangeStates1(event: any) {
    
  //   this.storeIds.forEach((sid) => {
  //   this.changedStores = [];
  //   this.storesDrp = "";
  //     var state = Object.keys(sid).toString();
  //     if(state === this.statesDrp)
  //      this.changedStores.push(sid);
  //   });
  //   this.storesDrp = this.changedStores[0][this.statesDrp][0];
  // }
  onChangePromoType(event: any) {
    console.log(this.promoTypeDrp);
    console.log(this.redeemedBy);
    if(this.promoTypeDrp == "Generic") {
      this.redeemedBy = "Any Member";
      this.redeem = true;
    } else if(this.promoTypeDrp == "Non-Personalized Group" || this.promoTypeDrp == "Non-Personalized Individual") {
      this.redeemedBy = "Anyone";
      this.redeem = true;
    } else {
      this.redeemedBy = "Anyone";
      this.redeem = false;
    }
  }

  showOptions(event:MatCheckboxChange): void {
    console.log(event.checked);
    for(var i = 0; i < this.countryAndStores.length; i++) {
      if(event.checked == true) {
        this.storeShow = true;
      } else {
        this.storeShow = false;
      }
    }
    
  }
  onChangePromoLevel(event:any) {
    console.log(this.promoLevelDrp);
    if(this.promoLevelDrp == "Store") {
        this.promolevelstate = true;
        this.promolevelstore = true;
        this.promolevelcountries = true;

    } else if(this.promoLevelDrp == "State"){
        this.promolevelstate = true;
        this.promolevelstore = false;
        this.promolevelcountries = true;
    } else if(this.promoLevelDrp == "Country") {
      this.promolevelstate = false;
      this.promolevelstore = false;
      this.promolevelcountries = true;
    } else {
      this.promolevelstate = false;
      this.promolevelstore = false;
      this.promolevelcountries = false;
    }
  }

  onChangeadLevel(event:any) {
    console.log(this.adLevelDrp);
    if(this.adLevelDrp == "Store") {
        this.adlevelstate = true;
        this.adlevelstore = true;
        this.adlevelcountries = true;

    } else if(this.adLevelDrp == "State"){
        this.adlevelstate = true;
        this.adlevelstore = false;
        this.adlevelcountries = true;
    } else if(this.adLevelDrp == "Country") {
      this.adlevelstate = false;
      this.adlevelstore = false;
      this.adlevelcountries = true;
    } else {
      this.adlevelstate = false;
      this.adlevelstore = false;
      this.adlevelcountries = false;
    }
  }
  public hasError = (controlName: string, errorName: string) =>{
    return this.createPromotionForm.controls[controlName].hasError(errorName);
  }
  createPromotion() {
    this.createPromotionForm = new FormGroup({
      promotionType: new FormControl(this.promoTypeDrp, [Validators.required]),
      promotionStatus: new FormControl(this.statusDrp, [Validators.required]),
      brand: new FormControl(this.brandDrp, [Validators.required]),
      promotionOverview: new FormControl(this.promoOverview),
      advertisingChannel: new FormControl(this.adChannelDrp, [Validators.required]),
      redeemedBy: new FormControl(this.redeemedBy, [Validators.required]),
      startDate: new FormControl(this.startdate.value.toString(), [Validators.required]),
      endDate: new FormControl(this.enddate.value.toString(), [Validators.required]),
      promotionChannel: new FormControl(this.channelDrp, [Validators.required]),
      onlinePromoCode: new FormControl(this.onlinePromoCode, [Validators.required]),
      articleId: new FormControl(this.articleId, [Validators.required]),
      discountType: new FormControl(this.discountType, [Validators.required]),
      discountAmount: new FormControl(this.discountAmount, [Validators.required]),
      minimumSpend: new FormControl(this.minimumSpend, [Validators.required]),
      couponDescription: new FormControl(this.couponDescription, [Validators.required]),
      receiptDescription: new FormControl(this.receiptDescription, [Validators.required]),
      termsAndConditions: new FormControl(this.termsAndConditions, [Validators.required]),
      promotionLocationLevel: new FormControl(this.promoLevelDrp, [Validators.required]),
      promotionLocationCountry: new FormControl(this.country, [Validators.required]),
      promotionLocationState: new FormControl(this.statesDrp, [Validators.required]),
      promotionLocationStore: new FormControl(this.promoLevelStore, [Validators.required]),
      posPromoCode: new FormControl(this.posPromoCode, [Validators.required]),
      advertisingLocationLevel: new FormControl(this.adLevelDrp, [Validators.required]),
      advertisingLocationCountry: new FormControl(this.adLocationCountry, [Validators.required]),
      advertisingLocationState: new FormControl(this.adLocationState, [Validators.required]),
      advertisingLocationStore: new FormControl(this.adLocationStore, [Validators.required]),
      ecomPromoCode: new FormControl(this.ecomPromoCode, [Validators.required])
    });
    console.log(this.createPromotionForm.value);
    this.promotionService.postCreatePromotion(this.createPromotionForm.value).subscribe(data => {alert("Succesfully Added Promotion Details")},Error => {alert("Failed While Adding Promotion Details")});
  }
  savePromotion(){
    this.router.navigate(['/confirmpromotion']);
  }
}


