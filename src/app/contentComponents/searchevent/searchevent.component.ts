import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-searchevent',
  templateUrl: './searchevent.component.html',
  styleUrls: ['./searchevent.component.scss']
})
export class SearcheventComponent implements OnInit {

  constructor(private router: Router) { }
  selected = 'select';
  startdate = new FormControl(new Date());
  enddate = new FormControl(new Date());
  eventsDrp: string = "";
  eventslist: any;
  filteredEvents: any[] = [];
  listAllEventNames: any[] = [];
  filterdEvnts: any[] = [];
  eventss: any[] = [];
  tableShow: boolean = false;
  disablePromo: boolean = true;
  events: any[] = [
    {
      "eventid": "123456",
      "eventName": "E10",
      "brand": "Spotlight-AU",
      "promoId": "P10001",
      "promoName": "$25 Off",
      "startDate": "01-01-2021",
      "endDate": "15-01-2021"
    },
    {
      "eventid": "123457",
      "eventName": "E10",
      "brand": "Spotlight-AU",
      "promoId": "P10002",
      "promoName": "$30 Off",
      "startDate": "16-01-2021",
      "endDate": "30-01-2021"
    },
    {
      "eventid": "123458",
      "eventName": "E10",
      "brand": "Spotlight-AU",
      "promoId": "P10003",
      "promoName": "$35 Off",
      "startDate": "01-02-2021",
      "endDate": "15-02-2021"
    },
    {
      "eventid": "123459",
      "eventName": "E10",
      "brand": "Spotlight-AU",
      "promoId": "P10004",
      "promoName": "$40 Off",
      "startDate": "16-02-2021",
      "endDate": "28-02-2021"
    },
    {
      "eventid": "123460",
      "eventName": "E10",
      "brand": "Spotlight-AU",
      "promoId": "P10005",
      "promoName": "$45 Off",
      "startDate": "01-03-2021",
      "endDate": "15-03-2021"
    },
    {
      "eventid": "123461",
      "eventName": "E11",
      "brand": "Spotlight-NZ",
      "promoId": "P10006",
      "promoName": "$25 Off",
      "startDate": "16-03-2021",
      "endDate": "30-03-2021"
    },
    {
      "eventid": "123462",
      "eventName": "E11",
      "brand": "Spotlight-NZ",
      "promoId": "P10007",
      "promoName": "$30 Off",
      "startDate": "01-04-2021",
      "endDate": "15-04-2021"
    },
    {
      "eventid": "123463",
      "eventName": "E11",
      "brand": "Spotlight-NZ",
      "promoId": "P10008",
      "promoName": "$35 Off",
      "startDate": "16-04-2021",
      "endDate": "30-04-2021"
    },
    {
      "eventid": "123464",
      "eventName": "E11",
      "brand": "Spotlight-NZ",
      "promoId": "P10009",
      "promoName": "$40 Off",
      "startDate": "01-05-2021",
      "endDate": "15-05-2021"
    },
    {
      "eventid": "123465",
      "eventName": "E11",
      "brand": "Spotlight-NZ",
      "promoId": "P10010",
      "promoName": "$45 Off",
      "startDate": "16-05-2021",
      "endDate": "30-05-2021"
    }
  ]
  brandDrpValues: any[] = ["Spotlight-AU", "Spotlight-NZ", "Spotlight-MY", "Spotlight-SG", "Anaconda-AU", "Mountain Design-AU", "Harris Scarfe-AU"]
  eventNameDrpValues: any[] = ["Christmas", "New Year"]
  ngOnInit(): void {

   
  this.filterEvents();
  console.log(this.eventsDrp);
  if(this.eventsDrp == "") {
    this.tableShow = false;
  } else {
    this.tableShow = true;
  }

  }

  onlyUnique(value: any, index: any, self: string | any[]) {
    return self.indexOf(value) === index;
  }
  
  filterEvents() {
      this.events.forEach((event) => {
        this.listAllEventNames.push(event.eventName);
      })
      console.log(this.listAllEventNames);
      var unique = this.listAllEventNames.filter(this.onlyUnique);
      console.log(unique);    
      this.filteredEvents = unique;
      console.log(this.filteredEvents);
  }

  onChangeEventID(event: any) {
    this.tableShow = true;
    this.disablePromo = false;
    console.log(this.eventsDrp);
    this.eventss = [];
    this.eventss = this.events;
    this.filterdEvnts = this.eventss.filter(
      item => item.eventName.toLowerCase().includes(this.eventsDrp.toLowerCase())
    );
    console.log(this.filterdEvnts);
    this.eventss = this.filterdEvnts;
  }

  navigateTo() {
    this.router.navigate(['/eventandpromotions']);
  } 

  
  // filterEvnts() {
  //   this.filterdEvnts = this.events.filter(
  //     item => item.eventName.toLowerCase().includes(this.selectedUser.toLowerCase())
  //   );
  //   console.log(this.filterdOptions);
  // }

}
