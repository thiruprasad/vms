import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';


@Component({
  selector: 'app-create-voucher',
  templateUrl: './create-voucher.component.html',
  styleUrls: ['./create-voucher.component.scss']
})
export class CreateVoucherComponent implements OnInit {

  constructor() { }

  selected = 'select';
  startdate = new FormControl(new Date());
  enddate = new FormControl(new Date());

  ngOnInit(): void {
  }

}
