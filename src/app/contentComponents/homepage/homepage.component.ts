import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CognitoService } from 'src/app/services/cognito.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

userdetails:any;
  constructor(private router: Router, private cognito: CognitoService) { }

  ngOnInit(): void {
    this.cognito.getUser()
    .then((user: any) => {
      this.userdetails = user;
      console.log(this.userdetails);
    });
  }

  searchEvent() {
    this.router.navigate(['/searchevents']);
  }

  createEvent() {
    this.router.navigate(['/srgevents']);
  }

  createPromotion(){
    this.router.navigate(['/createpromotion']);
  }

}
